let garage = {
  macchine: [
    {
      marca: "fiat",
      modello: "panda",
    },
    {
      marca: "fiat",
      modello: "500",
    },
    {
      marca: "opel",
      modello: "corsa",
    },
  ],
  aggiungiMacchineArray: [""],
  aggiungiMacchina(brand, model) {
    this.macchine.push({
      marca: brand,
      modello: model
    });
  },
  stampaPerMarca(brand) {    
    //let lista = "";     // la lista non viene modificata o utilizzata 
    
    // svuota aggiungiMacchineArray
    while (this.aggiungiMacchineArray.length > 0) {
      this.aggiungiMacchineArray.pop();
    }

    for (i = 0; i < this.macchine.length; i++) {
      if (this.macchine[i].marca == brand) {
        this.aggiungiMacchineArray.push(this.macchine[i].modello);
      } 
    }
    for (i=0; i<this.aggiungiMacchineArray.length; i++){
      console.log(`${this.aggiungiMacchineArray[i]} `);
    }
    if (this.aggiungiMacchineArray.length == 0) {
      console.log("Attenzione!!! Brand non nel garage");      //se la lughezza di "aggiungiMacchineArray" è uguale a 0 vuol dire che nel garage non c'è quella marca
    }
    //return lista;       //la lista non viene mai compilata in questa funzione
  },
  stampa(){
    let lista ="";
    for (i=0; i < this.macchine.length; i++) {
      lista = lista + `${this.macchine[i].marca} ${this.macchine[i].modello}\n`;
    }
    return lista;
  },
};
console.log(garage.stampa());
garage.aggiungiMacchina("fiat","punto");
garage.aggiungiMacchina("opel","astra");
garage.aggiungiMacchina("bmw","serie 1");
garage.aggiungiMacchina("bmw","serie 2");
console.log(garage.stampa());
garage.stampaPerMarca("bmw");
garage.stampaPerMarca("fiat");
garage.stampaPerMarca("ciao");